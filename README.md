# 1.01 Campus

**Voraussetzung**: Keine

**Ziel**: Auffrischung und Einarbeitung in neue Entwicklungswerkzeuge.

**Dauer**: < 45 Minuten

## Aufgabenstellung
Schreiben Sie ein Modell, welches Professoren, Assistenten und Fakultäten verwaltet. Mittels des Modells soll eine Menge an Fragen beanwortet werden können.

### Grundlegende Informationen:
- Ein Professor hat:
  - einen Namen
  - ein Alter
  - eine Zugehörigkeit zu einer Fakultät
  - ein Forschungsgebiet
  
- Ein Assistent hat:
  - einen Namen
  - ein Alter
  - eine Zugehörigkeit zu einer Fakultät
  - einen Professor als Vorgesetzten.
  
- Professoren und Assistenten sind Mitarbeiter der Fakultät.

- Eine Fakultät:
  - hat einen Namen
  - besteht aus Professoren und Assistenten.

### Fragestellungen an das Modell:

1. Welche Assistenten sind einem bestimmten Professor zugeordnet?
2. Welche Professoren haben einen Assistenten?
3. Wie viele Professoren gibt es in der Fakultät?
4. Wie viele Assistenten gibt es in der Fakultät?
5. Wie viele verschiedene Forschungsbereiche gibt es?